<?php

namespace Drupal\openid_connect_harid\Plugin\OpenIDConnectClient;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\openid_connect\Plugin\OpenIDConnectClientBase;

/**
 * HarID OpenID Connect client.
 *
 * Implements OpenID Connect Client plugin for HarID.
 *
 * @OpenIDConnectClient(
 *   id = "harid",
 *   label = @Translation("HarID")
 * )
 */
class OpenIDConnectHarIDClient extends OpenIDConnectClientBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'require_strong_session' => FALSE,
      'require_personal_code' => FALSE,
      'use_test_idp' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['require_strong_session'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require strong session'),
      '#description' => $this->t('If enabled, only users with strong sessions (ID-card, Mobile-ID or Smart-ID) would be allowed.'),
      '#default_value' => $this->configuration['require_strong_session'],
    ];
    $form['require_personal_code'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require personal code'),
      '#description' => $this->t('If enabled, only users with personal code would be allowed.'),
      '#default_value' => $this->configuration['require_personal_code'],
    ];
    $form['use_test_idp'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use test Identity Provider'),
      '#description' => $this->t('If enabled, test.harid.ee will be used instead of harid.ee.'),
      '#default_value' => $this->configuration['use_test_idp'],
    ];

    $url = 'https://harid.ee/en/pages/dev-info';
    $form['description'] = [
      '#markup' => '<div class="description">' . $this->t('Please follow <a href="@url" target="_blank">instructions</a> to setup HarID client.', ['@url' => $url]) . '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getClientScopes(): ?array {
    $scopes = ['openid', 'profile', 'email', 'roles'];
    $configuration = $this->getConfiguration();

    if (isset($configuration['require_strong_session']) && $configuration['require_strong_session']) {
      $scopes[] = 'session_type';
    }
    if (isset($configuration['require_personal_code']) && $configuration['require_personal_code']) {
      $scopes[] = 'personal_code';
    }

    return $scopes;
  }

  /**
   * Returns base URL for either live or test IdP service.
   *
   * @return string
   *   Base URL of IdP service
   */
  private function getBaseUrl(): string {
    return $this->configuration['use_test_idp'] ? 'https://test.harid.ee/et' : 'https://harid.ee/et';
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoints(): array {
    $base_url = $this->getBaseUrl();

    return [
      'authorization' => Url::fromUri($base_url . '/authorizations/new')->toString(FALSE),
      'token' => Url::fromUri($base_url . '/access_tokens')->toString(FALSE),
      'userinfo' => Url::fromUri($base_url . '/user_info')->toString(FALSE),
    ];
  }

}
