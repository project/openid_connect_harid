# HarID

## INTRODUCTION

A [HarID](https://harid.ee) client implementation for
[OpenID Connect](https://www.drupal.org/project/openid_connect) module.

Current implementation uses `openid profile email roles` scopes by default and
adds a few more depending on the current configuration:
* Enabling **Require strong session** settings adds `session_type` to the scopes
* Enabling **require_personal_code** setting adds `personal_code` to the scopes

Additional capability is automated process for setting user language preference
based on value of `ui_locales` if one is present (the values are set upon each
successful authentication). An additional check is applied if user locale
preference matches one of the enabled languages.

Enabling **strong session** requirement will not allow login for users that are
not logging in with ID-card or Mobile-ID for the current session. This makes
sure that there is a valid real person behind an account that authenticates.
Personal code is a bit similar in nature, though it does not require strong
session to be used for current authentication, the IdP service just has to have
that information available (this most probably just requires strong session
authentication to be used at least once).

A setting controls if test or live HarID is used, allowing the module to
be used for development and testing purposes.

## REQUIREMENTS

* Drupal version 9 to 11.
* PHP version 7.4+
* [OpenID Connect](https://www.drupal.org/project/openid_connect) module
version 3

## INSTALLATION

Install using downloaded module version package or using
[Composer](https://getcomposer.org/) by running
`composer require drupal/openid_connect_harid`.

# CONFIGURATION

1. Activate the module and enable the **HarID** client
2. Client configuration and management is made in the admin section. You could
use admin/config/people/openid-connect or more specifically
admin/config/people/openid-connect/add/harid as a shortcut
   - The callback **Redirect URL** can be found at the bottom of the client
   creation page and depends on the label
3. Register your service with [HarID](https://harid.ee/en) using
[this](https://harid.ee/en/pages/dev-info) guide

# TODO

* Use `roles` scope to automatically manage additional local roles based on
ones provided by the Identity Provider.
* It might be a good idea to use `.well-known/openid-configuration` and
extract values from it directly instead of just having them hard-coded.
