<?php

namespace Drupal\Tests\openid_connect_harid\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that HarID module is loaded and configurable.
 *
 * @group openid_connect_harid
 */
class HarIDClientSettingsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['openid_connect_harid'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->user = $this->drupalCreateUser(['administer openid connect clients']);
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that HarID client creation page exists and has all the fields.
   */
  public function testClientAddPageExistsAndHasNecessaryFields(): void {
    $this->drupalGet('admin/config/people/openid-connect/add/harid');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->fieldExists('label');
    $this->assertSession()->fieldValueEquals('label', '');

    $this->assertSession()->fieldExists('settings[client_id]');
    $this->assertSession()->fieldValueEquals('settings[client_id]', '');

    $this->assertSession()->fieldExists('settings[client_secret]');
    $this->assertSession()->fieldValueEquals('settings[client_secret]', '');

    $this->assertSession()->fieldExists('settings[iss_allowed_domains]');
    $this->assertSession()->fieldValueEquals('settings[iss_allowed_domains]', '');

    $this->assertSession()->fieldExists('settings[require_strong_session]');
    $this->assertSession()->checkboxNotChecked('settings[require_strong_session]');

    $this->assertSession()->fieldExists('settings[require_personal_code]');
    $this->assertSession()->checkboxNotChecked('settings[require_personal_code]');

    $this->assertSession()->fieldExists('settings[use_test_idp]');
    $this->assertSession()->checkboxNotChecked('settings[use_test_idp]');
  }

  /**
   * Tests that HarID client creation works as expected.
   */
  public function testClientAddPageSubmissionAndEditPageFieldValues(): void {
    $this->drupalGet('admin/config/people/openid-connect/add/harid');
    $page = $this->getSession()->getPage();
    $page->fillField('label', 'HarID');
    $page->fillField('id', 'harid');
    $page->fillField('settings[client_id]', 'client id');
    $page->fillField('settings[client_secret]', 'client secret');
    $page->fillField('settings[iss_allowed_domains]', 'test.harid.ee');
    $page->checkField('settings[require_strong_session]');
    $page->checkField('settings[require_personal_code]');
    $page->checkField('settings[use_test_idp]');
    $page->findButton('Create OpenID Connect client')->click();

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('admin/config/people/openid-connect');
    $this->assertSession()->pageTextContains('OpenID Connect client HarID has been added.');

    $this->drupalGet('admin/config/people/openid-connect/harid/edit');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldValueEquals('label', 'HarID');
    $this->assertSession()->fieldValueEquals('id', 'harid');
    $this->assertSession()->fieldValueEquals('settings[client_id]', 'client id');
    $this->assertSession()->fieldValueEquals('settings[client_secret]', 'client secret');
    $this->assertSession()->fieldValueEquals('settings[iss_allowed_domains]', 'test.harid.ee');
    $this->assertSession()->checkboxChecked('settings[require_strong_session]');
    $this->assertSession()->checkboxChecked('settings[require_personal_code]');
    $this->assertSession()->checkboxChecked('settings[use_test_idp]');
  }

}
